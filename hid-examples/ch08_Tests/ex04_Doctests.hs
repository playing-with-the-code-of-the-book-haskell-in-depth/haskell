import Prelude
import Test.DocTest

main :: IO ()
main = doctest ["-ihid-examples/ch08_Tests/iplookup-lib", "hid-examples/ch08_Tests/iplookup-lib/ParseIP.hs"]