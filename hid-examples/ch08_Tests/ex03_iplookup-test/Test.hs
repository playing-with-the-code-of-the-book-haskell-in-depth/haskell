import "tasty" Test.Tasty
import "tasty-hspec" Test.Tasty.Hspec
import "base" Prelude

import ParseIPSpec
import LookupIPSpec
import Props
import GoldenTests


main :: IO ()
main = do
  specs <- concat <$> mapM testSpecs
             [ parseIPSpecs
             , lookupIPSpecs
             ]
  goldens <- goldenTests
  defaultMain (testGroup "All Tests" [
                  testGroup "Specs" specs
                , testGroup "Properties" props
                , testGroup "Golden Tests" goldens
                ])
