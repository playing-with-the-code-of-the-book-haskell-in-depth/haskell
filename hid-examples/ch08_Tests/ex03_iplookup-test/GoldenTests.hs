module GoldenTests (goldenTests) where

import "filepath" System.FilePath
    ( normalise
    , takeBaseName
    , replaceExtension
    )
import "tasty" Test.Tasty
import "tasty-golden" Test.Tasty.Golden
import "base" Prelude

import LookupIP
import ParseIP


goldenTests :: IO [TestTree]
goldenTests = sequence [golden_lookupIP]

testsDir :: FilePath
testsDir = normalise "hid-examples/ch08_Tests/ex03_iplookup-test/data/"

golden_lookupIP :: IO TestTree
golden_lookupIP = testGroup "lookupIP" . map createTest
                  <$> findByExtension [".iprs"] testsDir

createTest :: String -> TestTree
createTest iprsf = goldenVsFile
                    (takeBaseName iprsf)
                    goldenf
                    outf
                    testAction
  where
    ipsf = replaceExtension iprsf ".ips"
    goldenf = replaceExtension iprsf ".out.golden"
    outf = replaceExtension iprsf ".out"
    testAction = do
      iprs <- parseValidIPRanges <$> readFile iprsf
      ips <- parseValidIPs <$> readFile ipsf
      writeBinaryFile outf $ reportIPs iprs ips
