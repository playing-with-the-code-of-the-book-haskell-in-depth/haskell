import Criterion.Main
    ( defaultMain
    , bench
    , whnf)
import Prelude

import IsPrime qualified as IP
import IsPrimeUnfolded qualified as IPU

isPrime :: Integer -> Bool
isPrime n = all notDividedBy [2 .. n `div` 2]
  where
    notDividedBy m = n `mod` m /= 0

primeNumber :: Integer
primeNumber = 16183

main :: IO ()
main = defaultMain [
    bench "isPrime (declarative)" $ whnf IP.isPrime primeNumber
  , bench "isPrime (unfolded)" $ whnf IPU.isPrime primeNumber
  , bench "isPrime (rewritten)" $ whnf isPrime primeNumber
  ]