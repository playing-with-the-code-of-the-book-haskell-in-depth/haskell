import safe "base" Control.Applicative
    ( Applicative ((<*>)
    , pure)
    )
import safe "base" Control.Monad
    ( ap
    , Monad ( (>>), (>>=))
    )
import safe "base" Data.Bifunctor
    ( Bifunctor (bimap)
    , first
    )
import safe "base" Data.Bool
    ( otherwise )
import safe "base" Data.Function
    ( (.) , ($)
    , flip
    )
import safe "base" Data.Functor
    ( (<$>)
    , fmap
    , Functor
    )
import safe "base" Data.Functor.Compose
    ( Compose (Compose) )
import safe "base" Data.List
    ( (++) )
import safe "base" Data.Ord
    ( (>) )
import safe "base" Data.Semigroup
    ( (<>) )
import      "foundation" Foundation
    ( Int )
import      "foundation" Foundation.Numerical
    ( (+), (-) )
import safe "base" System.IO
    ( IO
    , print
    , putStrLn
    )
import safe "base" Text.Show
    ( Show, show )

-- non-portable
import safe "base" Data.Kind
    ( Type )

type Stream ::
    (Type -> Type)
    -> (Type -> Type)
    -> Type
    -> Type
type role Stream nominal nominal nominal
data Stream f m r =
    Step (f (Stream f m r))
    | Effect (m (Stream f m r))
    | Return r

type Of :: Type -> Type -> Type
type role Of nominal nominal
data Of a b = a :> b
  deriving stock Show

instance Functor (Of a) where
  fmap f (a :> b) = a :> f b

instance Bifunctor Of where
  bimap f g (a :> b) = f a :> g b

each :: [e] -> Stream (Of e) m ()
each [] = Return ()
each (x:xs) = Step $ x :> each xs

ssum :: forall {m :: Type -> Type} {b}.
    Monad m =>
    Stream (Of Int) m b -> m (Of Int b)
ssum (Return r) = pure ((0 :: Int) :> r)
ssum (Effect m) = m >>= ssum
ssum (Step (e :> str)) = first (+ e) <$> ssum str

_printStream :: (Show e, Show r)
  => Stream (Of e) IO r
  -> IO ()
_printStream (Return r) =
  putStrLn $ "Result: " <> show r
_printStream (Effect m) = do
  putStrLn "Run action:"
  str <- m
  _printStream str
_printStream (Step (e :> str)) = do
  putStrLn $ "Element: " <> show e
  _printStream str

instance (Functor f, Monad m)
  => Functor (Stream f m) where
    fmap :: forall a b.
      (a -> b)
      -> Stream f m a
      -> Stream f m b
    fmap fun stream = loop stream
      where
        loop ::
          Stream f m a
          -> Stream f m b
        loop (Return r) = Return (fun r)
        loop (Effect m) = Effect (fmap loop m)
        loop (Step f) = Step (fmap loop f)

instance (Functor f, Monad m)
  => Monad (Stream f m) where
    (>>=) :: forall r r1.
      Stream f m r
      -> (r -> Stream f m r1)
      -> Stream f m r1
    stream >>= fun = loop stream
      where
        loop ::
          Stream f m r
          -> Stream f m r1
        loop (Return r) = fun r
        loop (Effect m) = Effect (fmap loop m)
        loop (Step f) = Step (fmap loop f)

instance (Functor f, Monad m)
  => Applicative (Stream f m) where
    pure r = Return r
    (<*>) = ap

_stream1 :: Stream (Of Int) IO ()
_stream1 = do
    yield 1
    effect (putStrLn "action 1")
    yield 2
    effect (putStrLn "action 2")
    yield 3
  where
    effect :: Monad m
        => m r
        -> Stream f m r
    effect eff = Effect $ Return <$> eff
    yield :: a -> Stream (Of a) m ()
    yield a = Step (a :> empty)
      where
        empty :: Stream f m ()
        empty = Return ()


maps :: forall f1 f2 m r.
    (Functor f1, Monad m)
    => (f1 (Stream f2 m r) -> f2 (Stream f2 m r))
    -> Stream f1 m r
    -> Stream f2 m r
maps fun = loop
  where
      loop ::
        Stream f1 m r
        -> Stream f2 m r
      loop (Return r) = Return r
      loop (Effect m) = Effect (fmap loop m)
      loop (Step f) = Step (fun (fmap loop f))

_mapOf :: Monad m
  => (a -> b)
  -> Stream (Of a) m r
  -> Stream (Of b) m r
_mapOf fun = maps (first fun)

_zipsWith :: forall f1 f2 f3 m r.
    Monad m
    =>  ((Stream f1 m r -> Stream f2 m r -> Stream f3 m r)
          -> f1 (Stream f1 m r)
          -> f2 (Stream f2 m r)
          -> f3 (Stream f3 m r)
        )
    -> Stream f1 m r
    -> Stream f2 m r
    -> Stream f3 m r
_zipsWith fun = loop
  where
      loop ::
        Stream f1 m r
        -> Stream f2 m r
        -> Stream f3 m r
      loop (Return r) _ = Return r
      loop _ (Return r) = Return r
      loop (Effect m) t = Effect $ fmap (flip loop t) m
      loop s (Effect n) = Effect $ fmap (loop s) n
      loop (Step fs) (Step gs) = Step $ fun loop fs gs


_zipPair :: Monad m
    => Stream (Of a) m r
    -> Stream (Of b) m r
    -> Stream (Of (a, b)) m r
_zipPair = _zipsWith fun
  where
      fun :: forall {t1} {t2} {b1} {a} {b2}.
          (t1 -> t2 -> b1)
          -> Of a t1
          -> Of b2 t2
          -> Of (a, b2) b1
      fun p (e1 :> x) (e2 :> y) = (e1, e2) :> (p x y)

_zips :: (Monad m, Functor f, Functor g)
    => Stream f m r
    -> Stream g m r
    -> Stream (Compose f g) m r
_zips = _zipsWith fun
  where
      fun :: forall
          {f :: Type -> Type}
          {g :: Type -> Type}
          {t1} {t2} {a}.
          (Functor f, Functor g)
          => (t1 -> t2 -> a)
          -> f t1 -> g t2
          -> Compose f g a
      fun p fx gy =
        Compose (fmap (\x -> fmap (\y -> p x y) gy) fx)



decompose :: (Monad m, Functor f)
    => Stream (Compose m f) m r
    -> Stream f m r
decompose = loop
  where
      loop :: forall
          {m :: Type -> Type}
          {f :: Type -> Type}
          {r}.
          (Monad m, Functor f)
          => Stream (Compose m f) m r
          -> Stream f m r
      loop (Return r) = Return r
      loop (Effect m) = Effect (fmap loop m)
      loop (Step (Compose mstr)) = Effect $ do
        str <- mstr
        pure (Step (fmap loop str))



mapsM :: (Monad m, Functor f, Functor g)
    => (forall x . f x -> m (g x))
    -> Stream f m r
    -> Stream g m r
mapsM fun =
  decompose . maps (Compose . fun)

withEffect :: forall t m a r.
  Monad m
  => (t -> m a)
  -> Stream (Of t) m r
  -> Stream (Of t) m r
withEffect eff = mapsM go
  where
      go :: forall {b}.
        Of t b -> m (Of t b)
      go p@(e :> _) = eff e >> pure p

_withPrinting :: Show a
    => Stream (Of a) IO r
    -> Stream (Of a) IO r
_withPrinting =
  withEffect (\e -> putStrLn ("Element: " ++ show e))

splitsAt :: (Monad m, Functor f)
    => Int
    -> Stream f m r
    -> Stream f m (Stream f m r)
splitsAt = loop
  where
      loop :: forall
          {m :: Type -> Type}
          {f :: Type -> Type}
          {r}.
          (Functor m, Functor f)
          =>Int
          -> Stream f m r
          -> Stream f m (Stream f m r)
      loop n stream
        | n > (0 :: Int) =
          case stream of
            Return r
              -> Return (Return r)
            Effect m
              -> Effect (fmap (loop n) m)
            Step f
              -> Step (fmap (loop (n-1)) f)
        | otherwise = Return stream

chunksOf :: forall
    {m0 :: Type -> Type}
    {f0 :: Type -> Type}
    {r0}.
    (Monad m0, Functor f0)
    => Int 
    -> Stream f0 m0 r0
    -> Stream (Stream f0 m0) m0 r0
chunksOf n = loop
  where
    cutChunk :: forall
        {m1 :: Type -> Type}
        {f1 :: Type -> Type}
        {r1}.
        (Monad m1, Functor f1)
        => Stream f1 m1 r1
        -> Stream f1 m1 (Stream (Stream f1 m1) m1 r1)
    cutChunk str =
      fmap loop (splitsAt (n-(1 :: Int)) str)

    loop :: forall
        {m2 :: Type -> Type}
        {f2 :: Type -> Type}
        {r2}.
        (Monad m2, Functor f2)
        => Stream f2 m2 r2
        -> Stream (Stream f2 m2) m2 r2
    loop (Return r) = Return r
    loop (Effect m) = Effect (fmap loop m)
    loop (Step fs) = Step (Step (fmap cutChunk fs))

main :: IO ()
main = do
  s :> () <- ssum
    $ withEffect print
    $ mapsM ssum
    $ chunksOf 2
    $ each [1,1,1,1,1]
  print s
