import safe "base" Control.Applicative
    ( (*>), (<*>), (<|>) )
import      "resourcet" Control.Monad.Trans.Resource
    ( runResourceT )
import safe "attoparsec" Data.Attoparsec.ByteString.Char8 as A
    ( char
    , endOfInput
    , endOfLine
    , manyTill
    , Parser
    , sepBy1
    , takeWhile
    )
import      "streaming-utils" Data.Attoparsec.ByteString.Streaming
    ( parsed )
import safe "base" Data.Bool
    ( (&&) )
import safe "bytestring" Data.ByteString
    ( ByteString )
--import Data.ByteString qualified as B
import safe "base" Data.Eq
    ( (/=) )
import safe "base" Data.Function
    ( (&), ($) )
import safe "base" Data.Functor
    ( (<$>)
    , void
    )
import safe "text" Data.Text
    ( Text )
import safe "text" Data.Text.Encoding qualified as T
    ( decodeUtf8 )
import safe "streaming" Streaming.Prelude qualified as S
    ( print )
import      "streaming-bytestring" Streaming.ByteString qualified as BS
    ( readFile )
import safe "base" System.Environment
    ( getArgs )
import safe "base" System.IO
    ( IO
    , putStrLn
    )


field :: Parser ByteString
field = A.takeWhile (\c -> c /= ',' && c /= '\r' && c /= '\n')

textField :: Parser Text
textField = T.decodeUtf8 <$> field

record :: Parser [Text]
record = textField `sepBy1` (char ',')

endOfFile :: Parser ()
endOfFile = endOfInput <|> endOfLine *> endOfInput

file :: Parser [[Text]]
file =
  (:) <$> record
      <*> manyTill (endOfLine *> record)
                   endOfFile

main :: IO ()
-- main = B.readFile "data/quotes.csv" >>= print . parseOnly file
main = do
  args <- getArgs
  case args of
    [fname] -> runResourceT $
            BS.readFile fname
        & parsed file
        & void
        & S.print
    _ -> putStrLn "Usage: csv-simple filename\n"
