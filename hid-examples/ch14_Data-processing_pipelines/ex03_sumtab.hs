import safe "base" Control.Applicative
    ( pure )
import safe "base" Control.Monad
    ( (>>=)
    , foldM
    , mapM_
    )
import safe "base" Data.Foldable
    ( sum )
import safe "base" Data.Function
    ( (.), ($) )
import safe "base" Data.Functor
    ( fmap )
import safe "streaming" Data.Functor.Of
    ( Of )
import safe "base" Data.List
    ( map )
import safe "extra" Data.List.Extra qualified as LE
    ( chunksOf )
import safe "base" Data.Monoid
    ( mconcat )
import safe "base" Data.Semigroup
    ( (<>) )
import      "foundation" Foundation
    ( Int )
import      "foundation" Foundation.Numerical
    ( (+) )
import safe "streaming" Streaming as S
    ( chunksOf
    , mapsM
    , Stream
    )
import safe "streaming" Streaming.Prelude qualified as S
    ( each
    , fst'
    , map
    , mapM_
    , mconcat
    , store
    , sum
    )
import safe "base" System.Environment
    ( getArgs )
import safe "base" System.IO
    ( IO
    , print
    )

-- non-portable
import safe "text" Data.Text
-- Portability: GHC
    ( Text )
import safe "text" Data.Text.IO as TIO
-- Portability: GHC
    ( putStrLn )
import      "text-show" TextShow
-- Portability: GHC
    ( showt )

withTab :: Int -> Text
withTab num = showt num <> "\t"

tabulateL :: Int -> [Int] -> [Text]
tabulateL cols ns = map mconcat $ LE.chunksOf cols $ map withTab ns

sumAndTabL :: Int -> [Int] -> IO Int
sumAndTabL cols ns = do
  mapM_ TIO.putStrLn $ tabulateL cols ns
  pure $ sum ns

sumAndTabL1 :: Int -> [Int] -> IO Int
sumAndTabL1 cols ns = foldM prtLineSum 0 $ LE.chunksOf cols ns
  where
    prtLineSum !acc xs = do
      TIO.putStrLn $ mconcat $ map withTab xs
      pure $ acc + sum xs

tabulateS :: Int -> Stream (Of Int) IO r -> Stream (Of Text) IO r
tabulateS cols str = mapsM S.mconcat $ S.chunksOf cols $ S.map withTab str

sumAndTabS :: Int -> Stream (Of Int) IO r -> IO Int
sumAndTabS cols =
  fmap S.fst' . S.mapM_ TIO.putStrLn . tabulateS cols . S.store S.sum

main :: IO ()
main = do
  args <- getArgs
  case args of
    ["-l"] -> sumAndTabL 3 [1..10] >>= print
    ["-l1"] -> sumAndTabL1 5 [1..300000] >>= print
    ["-s"] -> sumAndTabS 5 (S.each [1..300000]) >>= print
    _ -> TIO.putStrLn "Unsupported args"
