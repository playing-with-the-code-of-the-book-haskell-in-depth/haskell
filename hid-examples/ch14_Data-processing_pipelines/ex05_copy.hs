import safe "base" Control.Applicative
    ( pure )
import      "resourcet" Control.Monad.Trans.Resource
    ( runResourceT )
import safe "base" Data.Function
    ( (&), ($) )
import safe "streaming" Data.Functor.Of
    ( Of( (:>) ) )
import safe "base" Data.Int
    ( Int )
import safe "base" Data.Semigroup
    ( (<>) )
import      "streaming-bytestring" Streaming.ByteString as BS
    ( copy
    , length
    , readFile
    , writeFile
    )
import safe "base" System.Environment
    ( getArgs )
import safe "filepath" System.FilePath
    ( replaceBaseName
    , takeBaseName
    )
import safe "base" System.IO
    ( FilePath
    , IO
    , putStrLn
    )
import safe "base" Text.Show
    ( show )

_copyFile' :: FilePath -> FilePath -> IO Int
_copyFile' fIn fOut = do
  (len :> ()) <- runResourceT
               $ BS.writeFile fOut
               $ BS.length
               $ BS.copy
               $ BS.readFile fIn
  pure len


copyFile :: FilePath -> FilePath -> IO Int
copyFile fIn fOut = runResourceT $ do
  (len :> ()) <- BS.readFile fIn
               & BS.copy
               & BS.length
               & BS.writeFile fOut
  pure len

main :: IO ()
main = do
  [fp] <- getArgs
  let copyName = replaceBaseName fp (takeBaseName fp <> ".copy")
  len <- copyFile fp copyName

  putStrLn $ show len <> " bytes copied"

{-
Testing:

dd if=/dev/urandom of=temp_1GB_file bs=1 count=0 seek=1G
time cabal -v0 run copy -- temp_1GB_file
-}
