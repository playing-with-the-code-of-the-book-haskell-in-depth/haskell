import safe "base" Data.Char
    ( Char
    , ord
    )
import safe "base" Control.Monad
    ( (>>) )
import safe "base" Data.Function
    ( (.), ($) )
import safe "streaming" Data.Functor.Of
    ( Of )
import safe "streaming" Streaming
    ( Stream
    , MonadIO
    )
import safe "streaming" Streaming qualified as S
    ( liftIO )
import safe "streaming" Streaming.Prelude qualified as S
    ( copy
    , each
    , mapM_
    )
import safe "base" System.IO
    ( IO
    , print
    , putChar
    )

printChar :: Char -> IO ()
printChar c = putChar c >> putChar ' '

printCode :: Char -> IO ()
printCode = print . ord

printChars :: MonadIO m => Stream (Of Char) m r -> m r
printChars = S.mapM_ (S.liftIO . printChar)

printCodes :: MonadIO m => Stream (Of Char) m r -> m r
printCodes = S.mapM_ (S.liftIO . printCode)

main :: IO ()
main = printChars $ printCodes $ S.copy $ S.each "hello"
