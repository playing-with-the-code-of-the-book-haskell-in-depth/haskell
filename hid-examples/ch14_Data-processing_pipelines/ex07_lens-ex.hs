import safe "base" Data.Eq
    ( Eq )
import safe "base" Data.Foldable
    ( foldMap )
import safe "base" Data.Function
    ( (.), ($) )
import safe "base" Data.Semigroup
    ( appEndo
    , Endo ( Endo )
    )
import      "foundation" Foundation
    ( Double )
import      "foundation" Foundation.Numerical
    ( (+) )
import safe "base" System.IO
    ( IO
    , print
    )
import safe "base" Text.Show
    ( Show )

-- non-portable
import safe "lens" Control.Lens
    ( makeLenses
    , over
    , traverse
    )
import safe "base" Data.Kind
    ( Type )

type Point :: Type
data Point = Point {_x :: Double, _y :: Double}
  deriving stock (Show, Eq)

type Segment :: Type
data Segment = Segment {_beg :: Point, _end :: Point}
  deriving stock Show

makeLenses ''Point
makeLenses ''Segment

segs :: [Segment]
segs = [Segment (Point 0 0) (Point 100 0),
        Segment (Point 100 0) (Point 0 100),
        Segment (Point 0 100) (Point 0 0)]

_move :: Double -> [Segment] -> [Segment]
_move d = over (traverse . beg . x) (+ d)
             . over (traverse . beg . y) (+ d)
             . over (traverse . end . x) (+ d)
             . over (traverse . end . y) (+ d)

move' :: Double -> [Segment] -> [Segment]
move' d = compose [ over (traverse . point . coord) (+ d)
                  | point <- [beg, end],
                    coord <- [x, y]]
  where
    -- compose :: forall {a}. t0 (a -> a) -> a -> a
    compose = appEndo . (foldMap Endo)
    -- () added for clarity.
    -- _compose' = appEndo . foldEndo
      -- where
        -- foldEndo :: forall {a}. t0 (a -> a) -> Endo a
        -- foldEndo = foldMap Endo

main :: IO ()
main = print $ move' 100 segs