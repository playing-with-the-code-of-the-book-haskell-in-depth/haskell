import safe "base" Control.Applicative
    ( (<|>)
    , Applicative
    , pure
    )
import safe "base" Data.Bool
    ( (&&) )
import safe "base" Data.Eq
    ( (==) )
import safe "base" Data.Function
    ( ($) )
import safe "base" Data.Functor
    ( (<$>) )
import safe "base" Data.Int
    ( Int )
import safe "base" Data.Maybe
    ( Maybe (Just, Nothing) )
import safe "base" Data.Proxy
    ( Proxy ( Proxy ) )
import safe "base" Data.String
    ( String )
import safe "base" System.IO
    ( IO
    , putStrLn )
import safe "base" System.IO.Error
    ( ioError
    , userError )
import safe "base" Text.Show
    ( Show
    , show )

import safe "base" Prelude
-- GHC.Real
    ( Integral )

-- non-portable
import safe "base" Data.Kind
    ( Constraint
    , Type
    )
import safe "base" GHC.TypeLits
    ( KnownSymbol
    , Symbol
    , symbolVal
    )
import safe "base" Text.Read
-- uses Text.ParserCombinators.ReadP
    ( Read
    , readMaybe
    )

type Rating :: Type
data Rating = Bad | Good | Great
  deriving stock Show

type ServiceStatus :: Type
data ServiceStatus = Ok | Down
  deriving stock Show

type Get :: Type -> Type
type role Get nominal
data Get (a :: Type)

type Capture :: Type -> Type
type role Capture nominal
data Capture (a :: Type)

type (:<|>) :: Type -> Type -> Type
type role (:<|>) nominal nominal
data a :<|> b = a :<|> b
infixr 8 :<|>

type (:>) :: forall k. k -> Type -> Type
type role (:>) nominal nominal
data (a :: k) :> (b :: Type)
infixr 9 :>

type BookID :: Type
type BookID = Int

type BookInfoAPI :: Type
type BookInfoAPI = Get ServiceStatus
                   :<|> "title" :> Capture BookID :> Get String
                   :<|> "year" :> Capture BookID :> Get Int
                   :<|> "rating" :> Capture BookID :> Get Rating

type HandlerAction :: Type -> Type
type HandlerAction a = IO a

type Server :: Type -> Type
type family Server layout :: Type
type instance Server (Get a) = HandlerAction a
type instance Server (a :<|> b) = Server a :<|> Server b
type instance Server ((_ :: Symbol) :> r) = Server r
type instance Server (Capture a :> r) = a -> Server r

impl1 :: Server BookInfoAPI
impl1 = pure Ok
       :<|> title
       :<|> year
       :<|> rating
  where
    title :: forall {f :: Type -> Type} {p}.
        Applicative f =>
        p -> f String
    title _ = pure "Haskell in Depth"

    year :: forall {f :: Type -> Type} {a} {p}.
        (Applicative f, Integral a) =>
        p -> f a
    year _ = pure 2020

    rating :: forall {f :: Type -> Type} {p}.
        Applicative f =>
        p -> f Rating
    rating _ = pure Great

_impl2 :: Server BookInfoAPI
_impl2 = pure Down
        :<|> title
        :<|> year
        :<|> rating
  where
    notImplemented :: forall {a}.
        IO a
    notImplemented = ioError (userError "not implemented")

    title :: forall {p} {a}.
        p -> IO a
    title _ = notImplemented

    year :: forall {p} {a}.
        p -> IO a
    year _ = notImplemented

    rating :: forall {p} {a}.
        p -> IO a
    rating _ = notImplemented


encode :: Show a => IO a -> IO String
encode m = show <$> m

type Request :: Type
type Request = [String]

type HasServer :: Type -> Constraint
class HasServer layout where
  route :: Proxy layout -> Server layout -> Request -> Maybe (IO String)

instance Show a => HasServer (Get a) where
  route :: Proxy (Get a)
        -> HandlerAction a -> Request -> Maybe (IO String)
  route _ handler [] = Just (encode $ handler)
  route _ _       _  = Nothing

instance {-# OVERLAPS #-} HasServer (Get String) where
  route :: Proxy (Get String)
        -> IO String -> Request -> Maybe (IO String)
  route _ handler [] = Just handler
  route _ _       _  = Nothing

instance (HasServer a, HasServer b) => HasServer (a :<|> b) where
  route :: Proxy (a :<|> b)
        -> (Server a :<|> Server b) -> Request -> Maybe (IO String)
  route _ (handlera :<|> handlerb) xs =
        route (Proxy :: Proxy a) handlera xs
    <|> route (Proxy :: Proxy b) handlerb xs

instance (KnownSymbol s, HasServer r) => HasServer ((s :: Symbol) :> r) where
  route :: Proxy (s :> r)
        -> Server r -> Request -> Maybe (IO String)
  route _ handler (x : xs)
    | symbolVal (Proxy :: Proxy s) == x = route (Proxy :: Proxy r) handler xs
  route _ _       _                     = Nothing

instance (Read a, HasServer r) => HasServer (Capture a :> r) where
  route :: Proxy (Capture a :> r)
        -> (a -> Server r) -> [String] -> Maybe (IO String)
  route _ handler (x : xs) = do
    a <- readMaybe x
    route (Proxy :: Proxy r) (handler a) xs
  route _ _       _        = Nothing

get :: HasServer layout => Proxy layout -> Server layout -> [String] -> IO String
get proxy handler request = case route proxy handler request of
  Nothing -> ioError (userError "404")
  Just m  -> m

check :: Server BookInfoAPI -> IO ()
check impl = do
  b <- get (Proxy :: Proxy BookInfoAPI) impl []
  answer <- get (Proxy :: Proxy BookInfoAPI) impl ["year", "7548"]
  putStrLn (if b == "Ok" && answer == "2020"
            then "OK"
            else "Wrong answer!")

main :: IO ()
main = check impl1
