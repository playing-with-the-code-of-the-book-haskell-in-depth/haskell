import safe "base" Control.Applicative
    ( Applicative
    , pure
    )
import safe "base" Data.Bool
    ( (&&) )
import safe "base" Data.Eq
    ( (==) )
import safe "base" Data.Function
    ( ($) )
import safe "base" Data.Functor
    ( (<$>) )
import safe "base" Data.Int
    ( Int )
import safe "base" Data.Maybe
    ( Maybe (Just, Nothing) )
import safe "base" Data.String
    ( String )
import safe "base" System.IO
    ( IO
    , putStrLn )
import safe "base" System.IO.Error
    ( ioError
    , userError )
import safe "base" Text.Show
    ( Show
    , show )

import safe "base" Prelude
-- GHC.Real
    ( Integral )

-- non-portable
import safe "base" Data.Kind
    ( Type )
import safe "base" GHC.TypeLits
    ( Symbol )
import safe "base" Text.Read
-- uses Text.ParserCombinators.ReadP
    ( readMaybe )


type Rating :: Type
data Rating = Bad | Good | Great
  deriving stock Show

type ServiceStatus :: Type
data ServiceStatus = Ok | Down
  deriving stock Show

type Get :: Type -> Type
type role Get nominal
-- from GHC message (also for the nexts):
-- type role Get phantom
data Get (a :: Type)

type Capture :: Type -> Type
type role Capture nominal
data Capture (a :: Type)

type (:<|>) :: Type -> Type -> Type
type role (:<|>) nominal nominal
-- type role :<|> representational
data a :<|> b = a :<|> b
infixr 8 :<|>

type (:>) :: forall k. k -> Type -> Type
type role (:>) nominal nominal
data (a :: k) :> (b :: Type)
infixr 9 :>

type BookID :: Type
type BookID = Int

type BookInfoAPI :: Type
type BookInfoAPI = Get ServiceStatus
                   :<|> "title" :> Capture BookID :> Get String
                   :<|> "year" :> Capture BookID :> Get Int
                   :<|> "rating" :> Capture BookID :> Get Rating

type HandlerAction :: Type -> Type
type HandlerAction a = IO a

-- This type is not used anywhere
-- BookInfoAPIImpl is the same as Server BookInfoAPI
-- type BookInfoAPIImpl :: Type
-- type BookInfoAPIImpl = HandlerAction ServiceStatus
                       -- :<|> (BookID -> HandlerAction String)
                       -- :<|> (BookID -> HandlerAction Int)
                       -- :<|> (BookID -> HandlerAction Rating)

type Server :: Type -> Type
type family Server layout :: Type
type instance Server (Get a) = HandlerAction a
type instance Server (a :<|> b) = Server a :<|> Server b
type instance Server ((_ :: Symbol) :> r) = Server r
type instance Server (Capture a :> r) = a -> Server r


impl1 :: Server BookInfoAPI
impl1 = pure Ok
       :<|> title
       :<|> year
       :<|> rating
    where
        title :: forall {f :: Type -> Type} {p}.
            Applicative f =>
            p -> f String
        title _ = pure "Haskell in Depth"

        year :: forall {f :: Type -> Type} {a} {p}.
            (Applicative f, Integral a) =>
            p -> f a
        year _ = pure 2020

        rating :: forall {f :: Type -> Type} {p}.
                  Applicative f =>
                  p -> f Rating
        rating _ = pure Great

_impl2 :: Server BookInfoAPI
_impl2 = pure Down
        :<|> title
        :<|> year
        :<|> rating
    where
        notImplemented :: forall {a}.
            IO a
        notImplemented = ioError (userError "not implemented")

        title :: forall {p} {a}.
            p -> IO a
        title _ = notImplemented

        year :: forall {p} {a}.
            p -> IO a
        year _ = notImplemented

        rating :: forall {p} {a}.
            p -> IO a
        rating _ = notImplemented

type Request :: Type
type Request = [String]

encode :: Show a => IO a -> IO String
encode m = show <$> m

route :: Server BookInfoAPI -> Request -> Maybe (IO String)
route (root :<|> _) [] = pure $ encode $ root
route (_ :<|> title :<|> year :<|> rating) [op, bid'] = do
  bid <- readMaybe bid'
  case op of
    "title" -> pure $ title bid
    "year" -> pure $ encode $ year bid
    "rating" -> pure $ encode $ rating bid
    _ -> Nothing
route _ _ = Nothing

get :: Server BookInfoAPI -> Request -> IO String
get impl xs =
  case route impl xs of
    Just m -> m
    Nothing -> pure "Malformed request"

check :: Server BookInfoAPI -> IO ()
check impl = do
  b <- get impl []
  answer <- get impl ["year", "7548"]
  putStrLn (if b == "Ok" && answer == "2020"
            then "OK"
            else "Wrong answer!")

main :: IO ()
main = check impl1
