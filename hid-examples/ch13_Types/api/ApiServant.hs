import safe "base" Control.Applicative
    ( Applicative
    , pure )
import      "aeson" Data.Aeson
    ( ToJSON )
import safe "base" Data.Int
    ( Int )
import safe "base" Data.Proxy
    ( Proxy ( Proxy ) )
import safe "base" Data.String
    ( IsString )
import      "wai" Network.Wai
    ( Application )
import      "warp" Network.Wai.Handler.Warp
    ( run )
import      "servant" Servant.API.Alternative
    ( (:<|>) ((:<|>)) )
import      "servant" Servant.API.Capture
    ( Capture )
import      "servant" Servant.API.ContentTypes
    ( JSON
    , PlainText )
import      "servant" Servant.API.Sub
    ( (:>) )
import      "servant" Servant.API.Verbs
    ( Get )
-- import      "servant-blaze" Servant.HTML.Blaze
    -- ( HTML )
import      "servant-server" Servant.Server
    ( serve )
import      "servant-server" Servant.Server.Internal
    ( Server )
import safe "base" System.IO
    ( IO )
-- import      "blaze-html" Text.Blaze.Html5 qualified as H (b, Html, toHtml)
import safe "base" Text.Show
    ( Show )

import safe "base" Prelude
-- GHC.Real
    ( Integral )

-- non-portable
import safe "base" Data.Kind
    ( Type )
import safe "text" Data.Text
-- Portability: GHC
    ( Text )
import safe "base" GHC.Generics
    ( Generic )


type Rating :: Type
data Rating = Bad | Good | Great
  deriving stock (Show, Generic)
  deriving anyclass ToJSON

type ServiceStatus :: Type
data ServiceStatus = Ok | Down
  deriving stock (Show, Generic)
  deriving anyclass ToJSON

type BookID :: Type
type BookID = Int

type BookInfoAPI :: Type
type BookInfoAPI = Get '[JSON] ServiceStatus
                   :<|> "title" :> Capture "id" BookID :> Get '[PlainText] Text
                   :<|> "year" :> Capture "id" BookID :> Get '[JSON] Int
                   :<|> "rating" :> Capture "id" BookID :> Get '[JSON] Rating

impl :: Server BookInfoAPI
impl = pure Ok
       :<|> title
       :<|> year
       :<|> rating
  where
    -- title _ = pure $ H.toHtml $ H.b "Haskell in Depth"

    title :: forall {f :: Type -> Type} {a} {p}.
        (Applicative f, IsString a)
        => p -> f a
    title _ = pure "Haskell in Depth"

    year :: forall {f :: Type -> Type} {a} {p}.
        (Applicative f, Integral a)
        => p -> f a
    year _ = pure 2020

    rating :: forall {f :: Type -> Type} {p}. Applicative f => p -> f Rating
    rating _ = pure Great

bookInfoAPI :: Proxy BookInfoAPI
bookInfoAPI = Proxy

app :: Application
app = serve bookInfoAPI impl

main :: IO ()
main = run 8081 app
