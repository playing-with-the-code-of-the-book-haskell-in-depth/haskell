{-# LANGUAGE
    GeneralizedNewtypeDeriving,
    Trustworthy
#-}

import safe "aeson" Data.Aeson
import safe "bytestring" Data.ByteString qualified as B
import safe "base" Control.Exception
    ( IOException )
import safe "exceptions" Control.Monad.Catch
import safe "mtl" Control.Monad.Trans
import safe "text" Data.Text qualified as T
import safe "text"  Data.Text.IO qualified as TIO
import safe "optparse-applicative" Options.Applicative as Opt
import "base" Prelude
import safe "base" System.Exit
import safe "base" System.IO.Error
    ( isDoesNotExistError
    , ioeGetFileName
    )

import STExcept
import App
import ProcessRequest

data AppMode = FileInput FilePath | Interactive
data Params = Params
                AppMode -- mode
                FilePath -- config file

mkParams :: Opt.Parser Params
mkParams = Params <$> (fileInput <|> interactive) <*> config
  where
    fileInput = FileInput <$> strOption
                (long "file" <> short 'f' <>
                 metavar "FILENAME" <> help "Input file" )
    interactive = flag Interactive Interactive
                  (long "interactive" <> short 'i' <>
                   help "Interactive mode")
    config = strOption (long "conf" <> short 'c' <>
                 value "config.json" <>
                 showDefault <>
                 metavar "CONFIGNAME" <> help "Configuration file" )

withConfig :: Params -> IO ()
withConfig (Params appMode config) = do
    wauth <- eitherDecodeStrict <$> B.readFile config
    case wauth of
      Right wauth' -> runMyApp (run appMode) wauth'
      Left _ -> throwM ConfigError
  where
    run (FileInput fname) = liftIO (TIO.readFile fname)
                            >>= processMany . T.lines
    run Interactive = processInteractively

main :: IO ()
main = (execParser opts >>= withConfig)
       `catches` [Handler parserExit,
                  Handler printIOError,
                  Handler printOtherErrors]
  where
    opts =
      info (mkParams <**> helper)
           (fullDesc <>
            progDesc "Reports sunrise/sunset times for the specified location")
    parserExit :: ExitCode -> IO ()
    parserExit _ = pure ()
    printIOError :: IOException -> IO ()
    printIOError e
      | isDoesNotExistError e = do
           let mbfn = ioeGetFileName e
           putStrLn $ "File " ++ maybe "" id mbfn  ++ " not found"
      | otherwise = putStrLn $ "I/O error: " ++ show e
    printOtherErrors :: SomeException -> IO ()
    printOtherErrors = print
