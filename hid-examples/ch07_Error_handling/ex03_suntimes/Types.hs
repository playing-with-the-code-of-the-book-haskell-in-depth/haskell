{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE Trustworthy #-}

module Types where

import      "aeson" Data.Aeson
import safe "text" Data.Text
import safe "time" Data.Time ( Day )
import safe "base" GHC.Generics
import safe "base" Prelude


type Address = Text

data When = Now | On Day
  deriving Show

data GeoCoords = GeoCoords { lat :: Text,
                             lon :: Text }
  deriving (Show, Generic, FromJSON)

data SunTimes dt = SunTimes { sunrise :: dt,
                              sunset :: dt }
  deriving (Show, Generic, FromJSON)

data WebAPIAuth = WebAPIAuth { timeZoneDBkey :: Text,
                               email :: Text,
                               agent :: Text}
  deriving (Show, Generic, FromJSON)
