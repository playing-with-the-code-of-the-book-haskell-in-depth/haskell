{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE OverloadedStrings #-}

import "text" Data.Text
import "mtl" Control.Monad.Trans
import "transformers" Control.Monad.Trans.State
import "monad-logger" Control.Monad.Logger
import "base" Prelude

popAndLog :: LoggingT (StateT [Int] IO) ()
popAndLog = do
  _:xs <- lift get
  lift (put xs)
  $logDebug ("***" <> (pack $ show xs) <> "***")

logStateEx :: LoggingT (StateT [Int] IO) Int
logStateEx =  do
  popAndLog
  popAndLog
  pure 5

main :: IO ()
main = runStateT (runStdoutLoggingT logStateEx) [1,2,3] >>= print
