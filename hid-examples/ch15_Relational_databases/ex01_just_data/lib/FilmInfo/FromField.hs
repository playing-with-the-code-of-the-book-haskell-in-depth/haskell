{-# LANGUAGE Trustworthy #-}

module FilmInfo.FromField () where

import safe "base" Control.Applicative
    ( pure )
import safe "base" Data.Maybe
    ( Maybe ( Just, Nothing ) )
import      "postgresql-simple" Database.PostgreSQL.Simple.FromField
    ( FromField ( fromField )
    , ResultError ( ConversionFailed, UnexpectedNull )
    , returnError )
import safe "bytestring" Data.ByteString.Char8 qualified as B
    ( unpack )

import FilmInfo.Data
    ( Rating
    , toMaybeRating )

instance FromField Rating where
  fromField f Nothing = returnError UnexpectedNull f ""
  fromField f (Just bs) =
    case toMaybeRating bs of
      Nothing -> returnError ConversionFailed f (B.unpack bs)
      Just r -> pure r
