{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE RoleAnnotations #-}
{-# LANGUAGE StandaloneDeriving #-}
{-# LANGUAGE Trustworthy #-}

module FilmInfo.Data
    ( CatId
    , CatId' ( CatId )
    , FilmCategories ( FilmCategories )
    , FilmId
    , FilmId' ( FilmId )
    , FilmInfo
    , FilmInfo' ( FilmInfo, filmId, title, description, filmLength, rating )
    , FilmLength
    , FilmLength' ( FilmLength )
    , fromRating
    , printFilm
    , Rating ( NC17 )
    , toMaybeRating
    ) where

import safe "base" Data.Eq
    ( Eq )
import safe "base" Data.Function
    ( (.) )
import safe "base" Data.Int
    ( Int32 )
import safe "base" Data.Kind
    ( Type )
import safe "base" Data.Maybe
    ( Maybe ( Just, Nothing ) )
import safe "base" Data.Semigroup
    ( (<>) )
import safe "base" Data.String
    ( IsString )
import safe "text" Data.Text
    ( Text )
import safe "text" Data.Text.IO as T
    ( putStrLn )
-- import safe "base" Prelude ()
import safe "base" System.IO
    ( IO )
import safe "base" Text.Show
    ( Show )
import      "text-show" TextShow
    ( Builder
    , fromText
    , TextShow ( showb , showbList )
    , toText
    )

type FilmId' :: Type -> Type
type role FilmId' representational
newtype FilmId' a = FilmId a
type FilmId :: Type
type FilmId = FilmId' Int32

type CatId' :: Type -> Type
type role CatId' representational
newtype CatId' a = CatId a
type CatId :: Type
type CatId = CatId' Int32

type FilmLength' :: Type -> Type
type role FilmLength' representational
newtype FilmLength' a = FilmLength a
type FilmLength :: Type
type FilmLength = FilmLength' Int32

type Rating :: Type
data Rating = G | PG | PG13 | R | NC17
  deriving stock Show

fromRating :: IsString p => Rating -> p
fromRating G = "G"
fromRating PG = "PG"
fromRating PG13 = "PG-13"
fromRating R = "R"
fromRating NC17 = "NC-17"

toMaybeRating :: (Eq p, IsString p) => p -> Maybe Rating
toMaybeRating "G" = Just G
toMaybeRating "PG" = Just PG
toMaybeRating "PG-13" = Just PG13
toMaybeRating "R" = Just R
toMaybeRating "NC-17" = Just NC17
toMaybeRating _ = Nothing

type FilmInfo' :: Type -> Type -> Type -> Type -> Type -> Type
type role FilmInfo' representational representational representational representational representational
data FilmInfo' i t d l r = FilmInfo {
    filmId :: i
  , title :: t
  , description :: d
  , filmLength :: l
  , rating :: r
  }
type FilmInfo :: Type
type FilmInfo = FilmInfo' FilmId Text (Maybe Text) FilmLength (Maybe Rating)

type FilmCategories :: Type
data FilmCategories = FilmCategories FilmInfo [Text]

type PrintDesc :: Type
data PrintDesc = WithDescription | NoDescription

instance TextShow FilmLength where
  showb (FilmLength l) = showb l <> " min"

instance TextShow FilmInfo where
  showb = filmBuilder NoDescription

filmBuilder :: PrintDesc -> FilmInfo -> Builder
filmBuilder printDescr FilmInfo {..} =
  fromText title <> " (" <> showb filmLength
  <> case rating of
       Just r -> ", " <> fromRating r <> ")"
       Nothing -> ")"
  <> case (printDescr, description) of
       (WithDescription, Just desc) -> "\n" <> fromText desc
       _ -> ""

printFilm :: FilmInfo -> IO ()
printFilm = T.putStrLn . toText . filmBuilder WithDescription

instance TextShow FilmCategories where
  showb (FilmCategories f cats) = showb f <> "\n" <> showbList cats
