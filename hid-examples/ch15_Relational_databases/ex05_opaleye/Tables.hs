{-# LANGUAGE EmptyDataDecls #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE Trustworthy #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE UndecidableInstances #-}

module Tables where

import safe "profunctors" Data.Profunctor
    ( dimap )
import safe "product-profunctors" Data.Profunctor.Product
    ( p2 )
import safe "product-profunctors" Data.Profunctor.Product.Default
import safe "product-profunctors" Data.Profunctor.Product.TH
    ( makeAdaptorAndInstanceInferrable )
import safe "opaleye" Opaleye

import safe "base" Prelude

-- non-portable
import safe "text" Data.Text
-- Portability: GHC
    ( Text )

import safe "haskell-in-depth" FilmInfo.Data
import safe "haskell-in-depth" FilmInfo.FromField ()


data PGRating

instance DefaultFromField PGRating Rating where
  defaultFromField = fromPGSFromField

instance pgf ~ FieldNullable PGRating => Default ToFields Rating pgf where
  def = dimap fromRating
              (unsafeCoerceColumn . unsafeCast "mpaa_rating")
              (def :: ToFields Text (Field SqlText))

makeAdaptorAndInstanceInferrable "pFilmId" ''FilmId'
makeAdaptorAndInstanceInferrable "pFilmLength" ''FilmLength'
makeAdaptorAndInstanceInferrable "pFilmInfo" ''FilmInfo'

type FilmIdField = FilmId' (Field SqlInt4)
type FilmIdFieldWrite = FilmId' (Maybe (Field SqlInt4))

type FilmLengthField = FilmLength' (Field SqlInt4)

type FilmInfoField =
  FilmInfo'
    FilmIdField
    (Field SqlText)
    (FieldNullable SqlText)
    FilmLengthField
    (FieldNullable PGRating)

type FilmInfoFieldWrite =
  FilmInfo'
    FilmIdFieldWrite
    (Field SqlText)
    (FieldNullable SqlText)
    FilmLengthField
    (FieldNullable PGRating)


filmTable :: Table FilmInfoFieldWrite FilmInfoField
filmTable =
  table "film"
        (pFilmInfo FilmInfo {
              filmId = pFilmId $ FilmId $ tableField "film_id"
            , title = tableField "title"
            , description = tableField "description"
            , filmLength = pFilmLength $ FilmLength $ tableField "length"
            , rating = tableField "rating"
            })

makeAdaptorAndInstanceInferrable "pCatId" ''CatId'

type CatIdField = CatId' (Field SqlInt4)
type CatIdFieldWrite = CatId' (Maybe (Field SqlInt4))

categoryTable :: Table (CatIdFieldWrite, Field SqlText)
                       (CatIdField, Field SqlText)
categoryTable =
  table "category"
        (p2 ( pCatId $ CatId $ tableField "category_id"
            , tableField "name"))

filmCategoryTable :: Table (FilmIdField, CatIdField)
                           (FilmIdField, CatIdField)
filmCategoryTable =
  table "film_category"
        (p2 ( pFilmId $ FilmId $ tableField "film_id"
            , pCatId $ CatId $ tableField "category_id"))
