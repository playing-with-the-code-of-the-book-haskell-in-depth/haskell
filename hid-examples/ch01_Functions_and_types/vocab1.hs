import safe "base" Data.Bool
    ( not )
import safe "base" Data.Char
    ( isLetter )
import safe "base" Data.Function
    ((.), ($))
import safe "base" Data.List qualified as L
    ( sort
    )
import safe "base" Data.List qualified as L -- or Prelude
    ( filter
    , length
    , map
    )
import safe "base" Data.List.NonEmpty qualified as NE
    ( group
    , head
    )
import safe "text" Data.Text qualified as T 
    ( dropAround
    , null
    , toCaseFold
    , unwords
    , words
    )
import safe "text" Data.Text.IO qualified as TIO
    ( putStrLn
    , readFile
    )
-- import Prelude ()
import safe "base" System.Environment
    ( getArgs )
import safe "base" System.IO
    ( IO
    , print
    )


main :: IO ()
main = do
  [fname] <- getArgs
  text <- TIO.readFile fname
  let
    ws = L.map NE.head
      $ NE.group
      $ L.sort
      $ L.map T.toCaseFold
      $ L.filter (not . T.null)
      $ L.map (T.dropAround $ not . isLetter)
      $ T.words text
  TIO.putStrLn $ T.unwords ws
  print $ L.length ws
