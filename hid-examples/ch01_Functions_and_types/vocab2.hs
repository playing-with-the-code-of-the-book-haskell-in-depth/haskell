import safe "base" Data.Bool
    ( not )
import safe "base" Data.Char
    ( isLetter )
import safe "base" Data.Function
    ( (.), ($) )
import safe "base" Data.Int
    ( Int )
import safe "base" Data.Kind
    ( Type )
import safe "base" Data.List -- or Data.Text or Prelude
    ( filter
    , length
    , map
    )
import safe "base" Data.List
    ( sort
    , group
    )
import safe "text" Data.Text qualified as T
    ( dropAround
    , null
    , Text
    , toCaseFold
    , unlines
    , words
    )
import safe "text" Data.Text.IO qualified as TIO
    ( putStrLn -- also in System.IO
    , readFile -- also in System.IO
    )
import safe "base" Data.Tuple
    ( fst )
import safe "base" GHC.Err
    ( error )
-- import Prelude ()
import safe "base" System.Environment
    ( getArgs )
import safe "base" System.IO
    ( FilePath
    , IO
    , putStrLn
    )


type Entry :: Type
type Entry = (T.Text, Int)

type Vocabulary :: Type
type Vocabulary = [Entry]

extractVocab :: T.Text -> Vocabulary
extractVocab t = map buildEntry
              $ group
              $ sort ws
  where
    ws = map T.toCaseFold
      $ filter (not . T.null)
      $ map cleanWord
      $ T.words t
    buildEntry :: forall {a}.
        [a] -> (a, Int)
    buildEntry xs@(x:_) = (x, length xs)
    buildEntry _ = error "unexpected"
    cleanWord = T.dropAround (not . isLetter)

printAllWords :: Vocabulary -> IO ()
printAllWords vocab = do
  putStrLn "All words: "
  TIO.putStrLn
    $ T.unlines
    $ map fst vocab

processTextFile :: FilePath -> IO ()
processTextFile fname = do
  text <- TIO.readFile fname
  let vocab = extractVocab text
  printAllWords vocab

main :: IO ()
main = do
  args <- getArgs
  case args of
    [fname] -> processTextFile fname
    _ -> putStrLn "Usage: vocab2 filename"
