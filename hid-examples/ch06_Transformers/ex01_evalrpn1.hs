import System.Environment (getArgs)

import Prelude
    ( ($), (++), (>>=)
    , IO
    , mapM_
    , putStrLn
    , show
    , String
    )


import EvalRPN (evalRPN)

evalPrintExpr :: String -> IO ()
evalPrintExpr str = do
  let r = evalRPN str
  putStrLn $ str ++ " = " ++ show r

main :: IO ()
main = getArgs >>= mapM_ evalPrintExpr
