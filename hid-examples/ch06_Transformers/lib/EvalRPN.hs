module EvalRPN
    ( evalRPN ) where

import safe "base" Control.Applicative
    ( (<*>)
    , pure
    )
import safe "base" Control.Monad
    ( (>>), (>>=) )
import safe "base" Data.Function
    ( flip )
import safe "base" Data.Functor
    ( (<$>) )
import safe "base" Data.List
    ( head
    , tail
    )
import safe "base" Data.String
    ( String
    , words
    )
import safe "base" Data.Traversable
    ( traverse )
import      "foundation" Foundation
    ( Integer )
import      "foundation" Foundation.Numerical
    ( (+), (*), (-) )

-- non-portable
import safe "mtl" Control.Monad.State
-- multi-param classes, functional dependencies
    ( evalState
    , get
    , modify
    , put
    , State
    )
import safe "base" Data.Kind
    ( Type )
import safe "base" Text.Read
-- uses Text.ParserCombinators.ReadP
    ( read )

{-
   Function evalRPN evaluates an expression given
   in the reversed polish notation (RPN, postfix notation):

   evalRPN "2 3 +" ==> 5 (== "2 + 3")
   evalRPN "2 3 4 + *" ==> 14 (== 2 * (3 + 4))
   evalRPN "3 2 -" ==> 1 (== "3 - 2")
-}

type Stack :: Type
type Stack = [Integer]

type EvalM :: Type -> Type
type EvalM = State Stack

push :: Integer -> EvalM ()
push x = modify (x:)

pop :: EvalM Integer
pop = do
  xs <- get
  put (tail xs)
  pure (head xs)

evalRPN :: String -> Integer
evalRPN expr = evalState evalRPN' []
  where
    evalRPN' =
      traverse step (words expr)
      >> pop
    step "+" = processTops (+)
    step "*" = processTops (*)
    step "-" = processTops (-)
    step t  = push (read t)
    processTops op =
      flip op <$> pop <*> pop >>= push
