import "base" Data.Maybe
    ( Maybe ( Just, Nothing ) )
import "base" Data.Semigroup
    ( (<>) )
import Person
    ( homer
    , Person ( Person )
    , spj
    )
-- import Prelude ()
import "base" System.IO
    ( IO )
import "text-show" TextShow
    ( fromString
    , printT
    , TextShow ( showb )
    )


instance TextShow Person where
  showb (Person name Nothing) = fromString name
  showb (Person name (Just age)) =
    fromString name <> " (" <> showb age <> ")"

main :: IO ()
main = do
  printT homer
  printT spj
