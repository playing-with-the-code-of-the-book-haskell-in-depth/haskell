import "base" Data.List
    ( (++) )
import "base" Data.Maybe
    ( Maybe ( Just, Nothing ) )
import Person
    ( homer
    , Person ( Person )
    , spj
    )
-- import Prelude ()
import "base" System.IO
    ( IO
    , print
    )
import "base" Text.Show
    ( Show ( show ))


instance Show Person where
  show (Person name Nothing) = name
  show (Person name (Just age)) = name ++ " (" ++ show age ++ ")"

main :: IO ()
main = do
  print homer
  print spj
