import "base" Data.Eq
    ( Eq )
import Person
    ( homer
    , Person ( Person )
    , spj
    )
-- import Prelude ()
import "base" System.IO
    ( IO
    , print
    )
import "base" Text.Read
    ( Read )
import "base" Text.Show
    ( Show )


deriving stock instance Show Person
deriving stock instance Read Person
deriving stock instance Eq Person

main :: IO ()
main = do
  print homer
  print spj
