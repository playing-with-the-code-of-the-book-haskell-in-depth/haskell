module Person
    ( homer
    , Person ( Person )
    , spj
    ) where

import "base" Data.Int
    ( Int )
import "base" Data.Kind
    ( Type
    )
import "base" Data.Maybe
    ( Maybe ( Nothing, Just ) )
import "base" Data.String
    ( IsString ( fromString )
    , String
    )
-- import Prelude ()

type Person :: Type
data Person = Person String (Maybe Int)

instance IsString Person where
  fromString name = Person name Nothing

homer :: Person
homer = Person "Homer Simpson" (Just 39)

spj :: Person
spj = "Simon Peyton Jones"
