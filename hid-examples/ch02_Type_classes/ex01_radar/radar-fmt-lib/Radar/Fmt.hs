module Radar.Fmt
    ( Direction
    , orientMany
    , rotateMany
    , rotateManySteps
    ) where

import Fmt
    ( Buildable( build ) )
-- import Prelude ()
import Radar
    ( Direction
        ( North
        , East
        , South
        , West
        )
    , orientMany
    , rotateMany
    , rotateManySteps
    , Turn
        ( TNone
        , TLeft
        , TRight
        , TAround
        )
    )
import Text.Read
    ( Read )

deriving stock instance Read Direction
instance Buildable Direction where
    build North = "N"
    build East = "E"
    build South = "S"
    build West = "W"

deriving stock instance Read Turn
instance Buildable Turn where
    build TNone = "--"
    build TLeft = "<-"
    build TRight = "->"
    build TAround = "||"
