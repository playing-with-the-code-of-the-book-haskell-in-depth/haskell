import safe "base" Control.Applicative
    ( (<$>)
    , pure
    )
import safe "base" Control.Monad
    ( replicateM
    , when
    )
import safe "base" Data.Bool
    ( Bool -- ( True )
    , not
    )
import safe "base" Data.Eq
    ( (==) )
import safe "base" Data.Function
    ( ($) )
import safe "base" Data.Int
    ( Int )
import safe "base" Data.List qualified as L
    ( sort
    , nub
    )
import safe "base" Data.List  qualified as L  -- or Prelude
    ( and
    , map
    , unlines )
import safe "base" Data.List.NonEmpty
    ( NonEmpty ( (:|) )
    )
import safe "base" Data.List.NonEmpty qualified as NE
    ( fromList
    , toList
    )
import safe "base" Data.Ord
    ( Ord )
import safe "base" GHC.Enum
    ( fromEnum
    , maxBound
    , minBound
    , toEnum
    )
-- import Prelude ()
import safe Radar
    ( Direction
    , every
    , orient
    , orientMany
    , rotateMany
    , rotateMany'
    , rotateManySteps
    , Turn
        ( TNone
        , TLeft
        , TRight
        , TAround
        )
    )
import safe "base" System.Exit
    ( exitFailure )
import safe "base" System.IO
    ( FilePath
    , IO
    , writeFile
    )
import safe "random" System.Random
    ( getStdRandom
    , Uniform
    , uniform
    , UniformRange
    )
import safe "random" System.Random.Stateful
    ( uniformRM
    , uniformM)
import Text.Show
    ( Show
    , show
    )


instance UniformRange Turn where
  uniformRM (lo, hi) rng = do
    res <- uniformRM (fromEnum lo :: Int, fromEnum hi) rng
    pure $ toEnum res

instance Uniform Turn where
  uniformM rng = uniformRM (minBound, maxBound) rng

instance UniformRange Direction where
  uniformRM (lo, hi) rng = do
    res <- uniformRM (fromEnum lo :: Int, fromEnum hi) rng
    pure $ toEnum res

instance Uniform Direction where
  uniformM rng = uniformRM (minBound, maxBound) rng

uniformIO :: Uniform a => IO a
uniformIO = getStdRandom uniform

uniformsIO :: Uniform a => Int -> IO [a]
uniformsIO n = replicateM n uniformIO

randomTurns :: Int -> IO [Turn]
randomTurns = uniformsIO

randomDirections :: Int -> IO [Direction]
randomDirections = uniformsIO

_writeRandomFile :: Show a =>
                   Int -> (Int -> IO [a]) -> FilePath -> IO ()
_writeRandomFile n gen fname = do
  xs <- gen n
  writeFile fname $ L.unlines $ L.map show xs

deriving stock instance Ord Turn

test_allTurnsInUse :: Bool
test_allTurnsInUse = L.sort (L.nub [ orient d1 d2 | d1 <- every, d2 <- every ])
                      == every

test_rotationsMonoidAgree :: [Turn] -> Bool
test_rotationsMonoidAgree ts =
   L.and [ rotateMany d ts == rotateMany' d ts | d <- every ]

test_orientRotateAgree :: NonEmpty Direction -> Bool
test_orientRotateAgree ds@(d :| _) = (NE.toList ds) == rotateManySteps d (orientMany ds)

main :: IO ()
main = do
  ds <- NE.fromList <$> randomDirections 1000
  -- ^ fromList raises an error if given an empty list.
  ts <- randomTurns 1000
  when (not $ L.and
    [ test_allTurnsInUse
    , test_orientRotateAgree ds
    , test_rotationsMonoidAgree ts
    ]) exitFailure
