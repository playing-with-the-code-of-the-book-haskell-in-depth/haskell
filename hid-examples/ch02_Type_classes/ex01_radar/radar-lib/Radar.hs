module Radar
    ( Direction
        ( North
        , East
        , South
        , West
        )
    , every
    , orient
    , orientMany
    , rotateMany
    , rotateMany'
    , rotateManySteps
    , Turn
        ( TNone
        , TLeft
        , TRight
        , TAround
        )
    ) where

import safe "base" Data.Bool
    ( otherwise )
import safe "base" Data.Eq
    ((==)
    , Eq
    )
import safe "base" Data.Foldable
    ( foldl )
import safe "base" Data.Function
    ((.), ($)
    , flip
    , id
    )
import safe "base" Data.Kind
    ( Constraint
    , Type
    )
import safe "base" Data.List qualified as L
    ( filter
    , scanl
    , zipWith
    )
import safe "base" Data.List.NonEmpty qualified as NE
    ( fromList
    , head
    , toList
    )
import safe "base" Data.List.NonEmpty
    ( NonEmpty ( (:|) )
    )
import safe "base" Data.Monoid
    ( mconcat
    , Monoid( mempty )
    )
import safe "base" Data.Semigroup
    ( Semigroup( (<>) )
    )
import safe "base" GHC.Enum
    ( Bounded
    , Enum
    , enumFrom
    , maxBound
    , minBound
    , pred
    , succ
    )
-- import Prelude ()
import safe "base" Text.Show
    ( Show )

type CyclicEnum :: Type -> Constraint
class (Eq a, Enum a, Bounded a) => CyclicEnum a where
  cpred :: a -> a
  cpred d
    | d == minBound = maxBound
    | otherwise = pred d

  csucc :: a -> a
  csucc d
    | d == maxBound = minBound
    | otherwise = succ d

type Direction :: Type
data Direction = North | East | South | West
  deriving anyclass CyclicEnum
  deriving stock (Eq, Enum, Bounded, Show)

type Turn :: Type
data Turn = TNone | TLeft | TRight | TAround
  deriving stock (Eq, Enum, Bounded, Show)

instance Semigroup Turn where
  TNone <> t = t
  TLeft <> TLeft = TAround
  TLeft <> TRight = TNone
  TLeft <> TAround = TRight
  TRight <> TRight = TAround
  TRight <> TAround = TLeft
  TAround <> TAround = TNone
  t1 <> t2 = t2 <> t1

instance Monoid Turn where
  mempty = TNone

rotate :: Turn -> Direction -> Direction
rotate TNone = id
rotate TLeft = cpred
rotate TRight = csucc
rotate TAround = cpred . cpred

rotateMany :: Direction -> [Turn] -> Direction
rotateMany = foldl (flip rotate)

rotateMany' :: Direction -> [Turn] -> Direction
rotateMany' dir ts = rotate (mconcat ts) dir

rotateManySteps :: Direction -> [Turn] -> [Direction]
rotateManySteps = L.scanl (flip rotate)

every :: (Enum a, Bounded a) => [a]
every = enumFrom minBound
-- ^ enumFrom should have a variant returning NonEmpty, but that changes nothing.

orient :: Direction -> Direction -> Turn
orient d1 d2 = NE.head $ NE.fromList $ L.filter (\t -> rotate t d1 == d2) every
-- ^ Partial for the compiler.

orientMany :: NonEmpty Direction -> [Turn]
orientMany ds@(_ :| dst) = L.zipWith orient (NE.toList ds) dst
