import safe "base" Data.Function
    ( ($) )
import safe "base" Data.List qualified as L
    ( map
    )
import safe "base" Data.List.NonEmpty qualified as NE
    ( map
    )
import safe "base" Data.List.NonEmpty
    ( nonEmpty
    )
import safe "base" Data.Maybe
    ( Maybe ( Just, Nothing ) )
import safe "base" Data.Semigroup
    ( Semigroup( (<>) )
    )
import safe "base" Data.String
    ( lines )
import      "fmt" Fmt
    ( (+||), (||+)
    , fmt
    , fmtLn
    , nameF
    , unwordsF
    )
-- import Prelude ()
import safe Radar.Fmt
    ( Direction
    , orientMany
    , rotateMany
    , rotateManySteps
    )
import safe "base" System.Environment
    ( getArgs )
import safe "base" System.IO
    ( FilePath
    , IO
    , putStrLn
    , readFile
    )
import safe "base" Text.Read
    ( Read
    , read
    )


rotateFromFile ::
    Direction
    -> FilePath
    -> IO ()
rotateFromFile dir fname = do
  f <- readFile fname
  let
    turns :: Read b => [b]
    turns = L.map read $ lines f
    finalDir = rotateMany dir turns
    dirs = rotateManySteps dir turns
  fmtLn $ "Final direction: " +|| finalDir ||+ ""
  fmt $ nameF "Intermediate directions" (unwordsF dirs)

orientFromFile :: FilePath -> IO ()
orientFromFile fname = do
  f <- readFile fname
  case nonEmpty $ lines f of
    Nothing -> putStrLn "ERROR: empty list in file!"
    (Just d) -> fmt $ nameF "All turns" (unwordsF $ orientMany $ NE.map read d)

main :: IO ()
main = do
  args <- getArgs
  case args of
    ["-r", fname, dir] -> rotateFromFile (read dir) fname
    ["-o", fname] -> orientFromFile fname
    _ -> putStrLn $ "Usage: locator -o filename\n" <>
                    "       locator -r filename direction"
