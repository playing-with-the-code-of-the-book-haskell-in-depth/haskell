import safe "base" Data.Kind
    ( Constraint
    , Type
    )
-- import safe GHC.Read (readPrec)

import safe Prelude ( (==)
    , Eq
    , flip
    , id
    , IO
    , otherwise
    , print
    , Show
    , String
    )

-- import safe Text.ParserCombinators.ReadPrec
    -- ( minPrec
    -- , readPrec_to_S
    -- )


type Name :: Type
type Name = String
type Phone :: Type
type Phone = String
type Location :: Type
type Location = String
type PhoneNumbers :: Type
type PhoneNumbers = [(Name, Phone)]
type Locations :: Type
type Locations = [(Phone, Location)]


type Functor :: (Type -> Type) -> Constraint
class Functor f where
    fmap        :: (a -> b) -> f a -> f b
-- https://hackage.haskell.org/package/base-4.19.0.0/docs/src/GHC.Base.html

-- (<$>) :: Functor f => (a -> b) -> f a -> f b
-- (<$>) = fmap

type Applicative :: (Type -> Type) -> Constraint
class Functor f => Applicative f where
    -- pure :: a -> f a
    (<*>) :: f (a -> b) -> f a -> f b
    (<*>) = liftA2 id
    liftA2 :: (a -> b -> c) -> f a -> f b -> f c
    liftA2 f x = (<*>) (fmap f x)
-- https://hackage.haskell.org/package/base-4.19.0.0/docs/src/GHC.Base.html#Applicative

type Monad :: (Type -> Type) -> Constraint
class Applicative m => Monad m where
    (>>=)       :: forall a b. m a -> (a -> m b) -> m b

type Maybe :: Type -> Type
type role Maybe nominal
data  Maybe a  =  Nothing | Just a
    deriving stock (Show)
             -- Eq
             -- , Ord

instance  Functor Maybe  where
    fmap _ Nothing       = Nothing
    fmap f (Just a)      = Just (f a)

instance Applicative Maybe where
    -- pure = Just

    Just f  <*> m       = fmap f m
    Nothing <*> _m      = Nothing

    liftA2 f (Just x) (Just y) = Just (f x y)
    liftA2 _ _ _ = Nothing

instance  Monad Maybe  where
    (Just x) >>= k      = k x
    Nothing  >>= _      = Nothing

    -- (>>) = (*>)

lookup                  :: (Eq a) => a -> [(a,b)] -> Maybe b
lookup _key []          =  Nothing
lookup  key ((x,y):xys)
    | key == x           =  Just y
    | otherwise         =  lookup key xys
-- https://hackage.haskell.org/package/base-4.19.0.0/docs/src/GHC.List.html#lookup

_locateByName :: PhoneNumbers -> Locations -> Name -> Maybe Location
_locateByName pnumbers locs name =
  lookup name pnumbers >>= flip lookup locs


main :: IO ()
main = do
    print "The End"
-- https://hackage.haskell.org/package/base-4.19.0.0/docs/src/GHC.Base.html#Functor
-- Functor and its instance for Maybe
