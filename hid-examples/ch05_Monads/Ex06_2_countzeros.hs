import safe "base" Control.Monad
    ( when )
import safe "base" Data.Eq
    ( (==) )
import safe "base" Data.Foldable
    ( length
    , traverse_ )
import safe "base" Data.Function
    ( (.), ($) )
import safe "base" Data.List
    ( filter
    , replicate )
import      "foundation" Foundation
    ( Int )
import      "foundation" Foundation.Numerical
    ( (+) )
import safe "base" System.IO
    ( IO
    , print
    )

-- import safe "base" Prelude

-- non-portable
import safe "base" Control.Monad.ST
-- requires universal quantification for runST
    ( runST, ST )
import safe "base" Data.STRef
-- uses Control.Monad.ST
    ( modifySTRef'
    , newSTRef
    , readSTRef
    , STRef
    )

countZeros :: [Int] -> Int
countZeros = length . filter (== 0)

countZerosST :: [Int] -> Int
countZerosST xs = runST $ do
   c <- newSTRef 0
   traverse_ (\x -> when (x == 0) $ inc c) xs
   readSTRef c
 where
   inc :: forall {s}.
       STRef s Int
       -> ST s ()
   inc c = modifySTRef' c (+ (1 :: Int))

main :: IO ()
main = do
  print $ countZeros $ replicate 1000 0
  print $ countZerosST $ replicate 1000 0
