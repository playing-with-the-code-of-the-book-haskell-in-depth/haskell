import safe "base" Control.Applicative
    ( (<*>)
    , pure
    )
import safe "base" Control.Monad
    ( (>>), (>>=)
    , replicateM
    )
import safe "mtl" Control.Monad.RWS
    ( ask
    , evalRWS
    , get
    , put
    , RWS
    , state
    , tell
    )
import safe "base" Data.Function
    ( (.) )
import safe "base" Data.Functor
    ( (<$>) )
import safe "base" Data.Int
    ( Int )
import safe "base" Data.Kind
    ( Type )
-- import Prelude ()
import safe "base" System.IO
    ( IO
    , print
    )
import safe "random" System.Random
    ( newStdGen
    , StdGen
    , uniformR
    )

type Dice :: Type
type Dice = Int

type DiceGame :: Type -> Type
type DiceGame =
    RWS (Int, Int) [Dice] StdGen

_dice' :: DiceGame Dice
_dice' = do
  bs <- ask
  g <- get
  let (r, g') = uniformR bs g
  put g'
  tell [r]
  pure r

dice :: DiceGame Dice
dice = do
  bs <- ask
  r <- state (uniformR bs)
  tell [r]
  pure r

_dice'' :: DiceGame Dice
_dice'' =
    ask
    >>= state . uniformR
    >>= \r -> tell [r]
    >> pure r

doubleDice :: DiceGame (Dice, Dice)
doubleDice = (,) <$> dice <*> dice

dices :: Int -> DiceGame [Dice]
dices n = replicateM n dice

diceGame :: DiceGame (Dice, Dice)
diceGame = dice
    >> dices 5
    >> replicateM 2 (dices 3)
    >> dices 10
    >> doubleDice

main :: IO ()
main = newStdGen
    >>= print . evalRWS diceGame (1, 6)
