import safe Data.Kind ( Type )
import safe Text.Read ( readMaybe )

import safe Prelude ( (+), (*), (<$>), (<*>), (>>=)
    , flip
    , fmap
    , Int
    , IO
    , lookup
    , Maybe( Just, Nothing )
    , Num
    , print
    , Read
    , String
    )

type Name :: Type
type Name = String

type Phone :: Type
type Phone = String

type Location :: Type
type Location = String

type PhoneNumbers :: Type
type PhoneNumbers = [(Name, Phone)]

type Locations :: Type
type Locations = [(Phone, Location)]

doubleStrNumber1 :: (Num a, Read a) => String -> Maybe a
doubleStrNumber1 str =
  case readMaybe str of
    Just x -> Just (2 * x)
    Nothing -> Nothing

doubleStrNumber2 :: (Num a, Read a) => String -> Maybe a
doubleStrNumber2 s = (2 *) `fmap` readMaybe s


plusStrNumbers :: (Num a, Read a) => String -> String -> Maybe a
plusStrNumbers s1 s2 = (+) <$> readMaybe s1 <*> readMaybe s2

_locateByName :: PhoneNumbers -> Locations -> Name -> Maybe Location
_locateByName pnumbers locs name =
  lookup name pnumbers >>= flip lookup locs

_locateByName' :: PhoneNumbers -> Locations -> Name -> Maybe Location
_locateByName' pnumbers locs name =
  case lookup name pnumbers of
    Just number -> lookup number locs
    Nothing -> Nothing

main :: IO ()
main = do
  print (doubleStrNumber1 "21" :: Maybe Int)
  print (doubleStrNumber2 "21" :: Maybe Int)
  print (plusStrNumbers "10" "xx" :: Maybe Int)

