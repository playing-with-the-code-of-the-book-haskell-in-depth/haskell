import safe "base" Control.Applicative
    ( (<*>)
    , pure
    )
import safe "base" Control.Monad
    ( replicateM )
import safe "mtl" Control.Monad.State
-- Which one? 
  -- Control.Monad.Trans.State.Lazy
  -- Control.Monad.Trans.State.Strict
    ( evalState
    , State
    , state
    )
import safe "base" Data.Bool
    ( otherwise )
import safe "base" Data.Eq
    ( (==)
    , Eq
    )
import safe "base" Data.Function
    ( ($) )
import safe "base" Data.Functor
    ( (<$>) )
import safe "base" Data.Int
    ( Int )
import safe "base" Data.Kind
    ( Type )
import safe "base" Data.List
    ( group
    , length
    , map
    , sort
    )
import safe "base" Data.Ord
    ( Ord )
import safe "base" GHC.Enum
    ( Bounded
    , Enum
    , fromEnum
    , maxBound
    , minBound
    , toEnum
    )
import safe "base" GHC.Err
    ( error )
-- import Prelude ()
import safe "base" System.IO
    ( IO
    , print
    )
import safe "random" System.Random
    ( newStdGen
    , StdGen
    , Uniform
    , uniform
    , UniformRange
    )
import safe "random" System.Random.Stateful
    ( uniformRM
    , uniformM
    )
import safe "base" Text.Show
    ( Show )


type Weapon :: Type
data Weapon = Rock | Paper | Scissors
  deriving stock (Show, Bounded, Enum, Eq)

type Winner :: Type
data Winner = First | Second | Draw
  deriving stock (Show, Eq, Ord)

winner :: (Weapon, Weapon) -> Winner
winner (Paper, Rock) = First
winner (Scissors, Paper) = First
winner (Rock, Scissors) = First
winner (w1, w2)
  | w1 == w2 = Draw
  | otherwise = Second

instance UniformRange Weapon where
  uniformRM (lo, hi) rng = do
    res <- uniformRM (fromEnum lo :: Int, fromEnum hi) rng
    pure $ toEnum res

instance Uniform Weapon where
  uniformM rng = uniformRM (minBound, maxBound) rng

randomWeapon :: State StdGen Weapon
randomWeapon = state uniform

gameRound :: State StdGen (Weapon, Weapon)
gameRound = (,) <$> randomWeapon <*> randomWeapon

game :: Int -> State StdGen [(Winner, Int)]
game n = counts <$> replicateM n (winner <$> gameRound)
  where
    counts :: forall {a}. Ord a =>
        [a] -> [(a, Int)]
    counts xs = map headLength $ group $ sort xs

    headLength :: forall {a}.
        [a] -> (a, Int)
    headLength xs@(x:_) = (x, length xs)
    headLength [] = error "unexpected"

main :: IO ()
main = do
  g <- newStdGen
  let r = evalState (game 10) g
  print r
