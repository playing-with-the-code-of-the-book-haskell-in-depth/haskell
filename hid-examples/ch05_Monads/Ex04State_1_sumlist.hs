import Control.Monad.State
    ( execState
    , get
    , modify'
    , put
    , State
    )
import Data.Foldable (traverse_)
import Prelude
    ( (+)
    , Integer
    , IO
    , print
    )

addItem :: Integer -> State Integer ()
addItem n = do
  s <- get
  put (s + n)

_addItem' :: Integer -> State Integer ()
_addItem' n = modify' (+ n)

sumList :: [Integer] -> State Integer ()
sumList xs = traverse_ addItem xs

answer :: Integer
answer = execState (sumList [1..100]) 0

main :: IO ()
main = print answer
