module Main
    ( gcd
    , gcd_countSteps
    , gcd_logSteps
    , gcdM
    , main
    ) where

import safe "base" Control.Applicative
    ( pure )
import safe "base" Control.Monad
    ( (>>)
    , Monad
    )
import safe "mtl" Control.Monad.Writer
    ( mapWriter
    , tell
    , Writer
    )
import safe "base" Data.Foldable
    ( Foldable )
import safe "base" Data.Function
    ( (.), ($) )
import safe "base" Data.Functor
    ( (<$>) -- infix fmap
    )
import safe "base" Data.Int
    ( Int )
import safe "base" Data.Kind
    ( Type )
import safe "base" Data.List
    ( length )
import safe "base" Data.Monoid (Sum(Sum))
import safe "base" GHC.Real
    ( Integral
    , mod
    )
-- import Prelude ()
import safe "base" System.IO
    ( IO
    , print
    )
import safe "base" Text.Show
    ( Show )


gcd :: Integral a => a -> a -> a
gcd a 0 = a
gcd a b = gcd b (a `mod` b)

gcdM :: (Integral a, Monad m)
    => (a -> a -> m ())
    -> a
    -> a
    -> m a
gcdM step a 0 = step a 0 >> pure a
gcdM step a b = step a b >> gcdM step b (a `mod` b)

_gcd_print :: (Show a, Integral a)
    => a -> a -> IO a
_gcd_print = gcdM (\a b -> print (a, b))

gcd_countSteps :: Integral a
    => a -> a -> Writer (Sum Int) a
gcd_countSteps = gcdM (\_ _ -> tell $ Sum 1)

gcd_logSteps :: Integral a
    => a -> a -> Writer [(a, a)] a
gcd_logSteps = gcdM (\a b -> tell [(a, b)])

_gcd_countSteps' :: Integral a
    => a -> a -> Writer (Sum Int) a
_gcd_countSteps' a b = mapWriter mapper (gcd_logSteps a b)
  where
    mapper :: forall {t :: Type -> Type} {a1} {a2}.
        Foldable t
        => (a1, t a2)
        -> (a1, Sum Int)
    mapper (v, w) = (v, Sum $ length w)

_gcd_countSteps'' :: Integral a
    => a -> a -> Writer (Sum Int) a
_gcd_countSteps'' = (mapWriter (Sum . length <$>) .) . gcd_logSteps

main :: IO ()
main = print "OK"
