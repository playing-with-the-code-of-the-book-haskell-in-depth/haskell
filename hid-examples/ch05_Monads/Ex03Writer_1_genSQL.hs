import safe "base" Control.Applicative
    ( pure )
import safe "base" Control.Monad
    ( (>>) )
import safe "transformers" Control.Monad.Trans.Writer.Lazy
    ( WriterT )
import safe "mtl" Control.Monad.Writer
    ( runWriter
    , tell
    , Writer
    )
import safe "base" Data.Foldable
    ( traverse_ -- (Foldable t, Applicative f) => (a -> f b) -> t a -> f ()
    )
import safe "base" Data.Function
    ( ($) )
import safe "base" Data.Functor
    ( (<$>) -- infix fmap
    )
import safe "base" Data.Functor.Identity
    ( Identity )
import safe "base" Data.Int
    ( Int )
import safe "base" Data.Kind
    ( Type )
import safe "base" Data.List
    ( zip )
import safe "base" Data.Semigroup
    ( (<>) )
import safe "text" Data.Text
    ( Text )
import safe "text" Data.Text qualified as T
    ( concat -- [Text] -> Text
    , lines
    , splitOn
    )
import safe "text" Data.Text.IO qualified as TIO 
    ( putStr
    , putStrLn
    )
import safe "base" Data.Traversable
    ( traverse -- (Traversable t, Applicative f) => (a -> f b) -> t a -> f (t b)
    )
-- import Prelude ()
import safe "base" System.IO
    ( IO
    , print
    )
import safe "base" Text.Show
    ( Show )


type SQL :: Type
type SQL = Text

type ErrorMsg :: Type
data ErrorMsg = WrongFormat Int Text
  deriving stock Show

genInsert :: Text -> Text -> Text
genInsert s1 s2 = "INSERT INTO items VALUES ('" <> s1 <> "','" <> s2 <> "');\n"

processLine ::
    (Int, Text)
    -> Writer [ErrorMsg] SQL
processLine (_, T.splitOn ":" -> [s1, s2]) = pure $ genInsert s1 s2
processLine (i, s) = tell [WrongFormat i s] >> pure ""

genSQL ::
    Text
    -> Writer [ErrorMsg] SQL
genSQL txt = T.concat <$> traverse processLine (zip [1..] $ T.lines txt)

halfGenSQL ::
    Text
    -> WriterT [ErrorMsg] Identity [SQL]
halfGenSQL txt = traverse processLine (zip [1..] $ T.lines txt)

testData :: Text
testData = "Pen:Bob\nGlass:Mary:10\nPencil:Alice\nBook:Bob\nBottle"

testGenSQL :: IO ()
testGenSQL = do
  let (sql, errors) = runWriter (genSQL testData)
  TIO.putStrLn "SQL:"
  TIO.putStr sql
  TIO.putStrLn "Errors:"
  traverse_ print errors

main :: IO ()
main = do
  print ("Half processed computation:" :: Text)
  print $ runWriter (halfGenSQL testData)
  testGenSQL
