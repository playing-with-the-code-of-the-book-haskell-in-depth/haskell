import safe "base" Data.Kind
    ( Constraint
    , Type )

import safe "base" Prelude ( (*)
    , Int
    , IO
    , Num
    , print
    , Read
    , Show
    , String
    )

import safe "base" Text.Read
    ( readPrec )
import safe "base" Text.ParserCombinators.ReadPrec
    ( minPrec
    , readPrec_to_S
    )


type Functor :: (Type -> Type) -> Constraint
class Functor f where
    fmap        :: (a -> b) -> f a -> f b
-- https://hackage.haskell.org/package/base-4.19.0.0/docs/src/GHC.Base.html

type Maybe :: Type -> Type
type role Maybe nominal
data  Maybe a  =  Nothing | Just a
    deriving stock (Show)
             -- Eq
             -- , Ord

instance  Functor Maybe  where
    fmap _ Nothing       = Nothing
    fmap f (Just a)      = Just (f a)

readMaybe :: Read a => String -> Maybe a
readMaybe s = case [ x | (x,"") <- readPrec_to_S readPrec minPrec s ] of
        [x] -> Just x
        _   -> Nothing

doubleStrNumber2 :: (Num a, Read a) => String -> Maybe a
doubleStrNumber2 s = (2 *) `fmap` readMaybe s

main :: IO ()
main = do
    print (doubleStrNumber2 "21" :: Maybe Int)

-- https://hackage.haskell.org/package/base-4.19.0.0/docs/src/GHC.Base.html#Functor
-- Functor and its instance for Maybe
