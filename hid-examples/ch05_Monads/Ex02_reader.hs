import safe "base" Control.Applicative
    ( pure )
import safe "base" Control.Monad
    ( when )
import safe "mtl" Control.Monad.Reader
    ( asks
    , local
    , Reader
    , runReader
    )
import safe "base" Data.Bool
    ( Bool ( True, False ) )
import safe "base" Data.Kind
    ( Type )
-- import safe Prelude ()
import safe "base" System.IO
    ( IO
    , print
    )


type Config :: Type
data Config = Config {
    verbose :: Bool
    {- other parameters -}
  }

type ConfigM :: Type -> Type
type ConfigM = Reader Config

getConfiguration :: IO Config
getConfiguration = pure Config { verbose = True {- ... -} }

main :: IO ()
main = do
  config <- getConfiguration
  let result = runReader work config
  print result

work :: ConfigM ()
work = do
  -- ...
  doSomething
  -- ...

doSomething :: ConfigM ()
doSomething = do
  -- ...
  doSomethingSpecial
  -- ...

doSomethingSpecial :: ConfigM ()
doSomethingSpecial = do
  -- ...
  -- Config {verbose} <- ask
  vrb <- asks verbose
  when vrb beVerbose
  -- ...

beVerbose :: ConfigM ()
beVerbose = pure ()


_doSomethingSpecialSilently :: ConfigM ()
_doSomethingSpecialSilently = local silent doSomethingSpecial
  where
    silent :: Config -> Config
    silent config = config {verbose = False}
