import safe "base" Control.Monad
    ( (>>), (>>=) )
import safe "base" Data.Functor
    ( (<$>) )
import safe "base" Data.IORef
    ( IORef
    , modifyIORef'
    , newIORef
    , readIORef)
import safe "base" Data.Maybe
    ( Maybe (Just, Nothing) )
import      "foundation" Foundation
    ( Int )
import      "foundation" Foundation.Numerical
    ( (+), Additive )
import safe "base" System.IO
    ( getLine
    , IO
    , print
    , putStr
    )

-- non-portable
import safe "base" Text.Read
-- uses Text.ParserCombinators.ReadP
    ( Read
    , readMaybe
    )

sumNumbers :: IO Int
sumNumbers = do
    s <- newIORef 0
    go s
  where
    go :: forall {a}.
        (Read a, Additive a) =>
        IORef a -> IO a
    go acc = readNumber >>= processNumber acc

    readNumber :: Read a =>
        IO (Maybe a)
    readNumber = do
      putStr "Put integer number (not a number to finish): "
      readMaybe <$> getLine

    processNumber :: forall {a}.
        (Read a, Additive a) =>
        IORef a -> Maybe a -> IO a
    processNumber acc Nothing = readIORef acc
    processNumber acc (Just n) = modifyIORef' acc (+ n) >> go acc

main :: IO ()
main = do
  s <- sumNumbers
  putStr "Your sum is: "
  print s
