import safe "base" Data.Kind
    ( Constraint
    , Type )
import safe "base" GHC.Read
    ( readPrec )

import safe "base" Prelude
    ( (+)
    , Int
    , IO
    , Num
    , print
    , Read
    , Show
    , String
    )

import safe "base" Text.ParserCombinators.ReadPrec
    ( minPrec
    , readPrec_to_S
    )


id                      :: a -> a
id x                    =  x
-- https://hackage.haskell.org/package/base-4.19.0.0/docs/src/GHC.Base.html

type Functor :: (Type -> Type) -> Constraint
class Functor f where
    fmap        :: (a -> b) -> f a -> f b
-- https://hackage.haskell.org/package/base-4.19.0.0/docs/src/GHC.Base.html

(<$>) :: Functor f => (a -> b) -> f a -> f b
(<$>) = fmap

type Applicative :: (Type -> Type) -> Constraint
class Functor f => Applicative f where
    -- pure :: a -> f a
    (<*>) :: f (a -> b) -> f a -> f b
    (<*>) = liftA2 id
    liftA2 :: (a -> b -> c) -> f a -> f b -> f c
    liftA2 f x = (<*>) (fmap f x)
-- https://hackage.haskell.org/package/base-4.19.0.0/docs/src/GHC.Base.html#Applicative


type Maybe :: Type -> Type
type role Maybe nominal
data  Maybe a  =  Nothing | Just a
    deriving stock (Show)
             -- Eq
             -- , Ord

instance  Functor Maybe  where
    fmap _ Nothing       = Nothing
    fmap f (Just a)      = Just (f a)

instance Applicative Maybe where
    -- pure = Just

    Just f  <*> m       = fmap f m
    Nothing <*> _m      = Nothing

    liftA2 f (Just x) (Just y) = Just (f x y)
    liftA2 _ _ _ = Nothing

readMaybe :: Read a => String -> Maybe a
readMaybe s = case [ x | (x,"") <- readPrec_to_S readPrec minPrec s ] of
        [x] -> Just x
        _   -> Nothing

plusStrNumbers :: (Num a, Read a) => String -> String -> Maybe a
plusStrNumbers s1 s2 = (+) <$> readMaybe s1 <*> readMaybe s2

main :: IO ()
main = do
    print (plusStrNumbers "3" "5" :: Maybe Int)
    print (plusStrNumbers "3" "x" :: Maybe Int)

-- https://hackage.haskell.org/package/base-4.19.0.0/docs/src/GHC.Base.html#Functor
-- Functor and its instance for Maybe
