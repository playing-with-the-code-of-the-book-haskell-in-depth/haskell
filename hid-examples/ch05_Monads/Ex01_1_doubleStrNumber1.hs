import safe Data.Kind (Type)
import safe GHC.Read (readPrec)
import safe Prelude ( (*)
    , Int
    , IO
    , Num
    , print
    , Read
    , Show
    , String
    )

import safe Text.ParserCombinators.ReadPrec
    ( minPrec
    , readPrec_to_S
    )

type Maybe :: Type -> Type
type role Maybe nominal
data Maybe a = Nothing | Just a
    deriving stock (Show)
             -- Eq
             -- , Ord

readMaybe :: Read a => String -> Maybe a
readMaybe s = case [ x | (x,"") <- readPrec_to_S readPrec minPrec s ] of
        [x] -> Just x
        _   -> Nothing

doubleStrNumber1 :: (Num a, Read a) => String -> Maybe a
doubleStrNumber1 str =
    case readMaybe str of
        Just x -> Just (2 * x)
        Nothing -> Nothing

main :: IO ()
main = do
    print (doubleStrNumber1 "21" :: Maybe Int)

