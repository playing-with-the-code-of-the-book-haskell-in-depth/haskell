import "async" Control.Concurrent.Async
import "stm" Control.Concurrent.STM
import "base" Control.Monad
import "base" Prelude
import "base" System.Environment


oneSec :: Int
oneSec = 1000000

printDots :: IO ()
printDots = forever $ do
  putStrLn "."
  tv <- registerDelay oneSec
  atomically $ readTVar tv >>= check

main :: IO ()
main = do
  [sec] <- getArgs
  withAsync printDots $ \_ -> do
    tv <- registerDelay $ oneSec * read sec
    atomically $ readTVar tv >>= check
  putStrLn "Alarm!"
