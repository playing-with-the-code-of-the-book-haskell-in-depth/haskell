import safe "base" Control.Monad

import safe "base" Prelude

-- non-portable
import safe "base" Control.Concurrent
  -- concurrency


oneSec :: Int
oneSec = 1000000

printHello :: IO ()
printHello = forever $ do
  putStr "Hello "
  putStrLn "world"

main :: IO ()
main = do
  _ <- replicateM 5 (forkIO printHello)
  threadDelay oneSec
