import safe "base" Control.Monad
    ( forever )
import safe "base" System.IO
    ( hSetBuffering
    , stdout
    , BufferMode (..) )

import safe "base" Prelude

-- non-portable
import safe "base" Control.Concurrent
    -- concurrency
    ( threadDelay )
import safe "async" Control.Concurrent.Async
    -- requires concurrency
    ( withAsync )

oneSec :: Int
oneSec = 1000000

doSomethingUseful :: IO ()
doSomethingUseful = do
  threadDelay $ 5 * oneSec
  putStrLn "All done"

printDots :: Int -> IO ()
printDots msec = forever $ do
  putStrLn "."
  threadDelay msec

main :: IO ()
main = do
  hSetBuffering stdout NoBuffering
  putStrLn "Start doing something useful"
  withAsync (printDots oneSec) $ \_ -> doSomethingUseful
  putStrLn "Exiting..."
