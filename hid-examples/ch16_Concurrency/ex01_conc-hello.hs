import safe "base" Prelude

-- non-portable
import safe "base" Control.Concurrent
-- concurrency
    ( getNumCapabilities )

main :: IO ()
main = do
  n <- getNumCapabilities
  putStrLn $ "This program runs over " ++ show n ++ " capabilities"
