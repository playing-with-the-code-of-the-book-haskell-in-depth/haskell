# haskell

Playing with the code of the book "Haskell in Depth" May 2021 by Vitaly Bragilevsky, from notes reading the book and from files of the repository "https://github.com/bravit/hid-examples".

File from the directory hid-examples where inspired from the repository "https://github.com/bravit/hid-examples" and most of the time adapted for newer versions of GHC or of the used libraries or to play with the code.

The table of content is taken from the web page of the book: https://www.manning.com/books/haskell-in-depth

# Warnings: options, messages, and occurence
## `orphans`
Orphan instance: Suggested fix: Move the instance declaration to the module of the class or of the type, or wrap the type with a newtype and declare the instance on the new type.
* radar (Main.hs)
* radar-test (Test.hs)

### Documentation
* [Orphan instance Suggested fix Move the instance declaration to the module of the class or of the type or wrap the type with a newtype and declare the instance on the new type](https://google.com/search?q=Orphan+instance+Suggested+fix+Move+the+instance+declaration+to+the+module+of+the+class+or+of+the+type+or+wrap+the+type+with+a+newtype+and+declare+the+instance+on+the+new+type)
* [Orphan instance](https://wiki.haskell.org/Orphan_instance) (wiki.haskell.org)


# Commented table of content
...
## 2 Type classes
### 2.1 Manipulating a radar antenna with type classes
#### 2.1.1 The problem at hand
#### 2.1.2 Rotating a radar antenna with Eq, Enum, and Bounded
#### 2.1.3 Combining turns with Semigroup and Monoid
#### 2.1.4 Printing and reading data with Show and Read
#### 2.1.5 Testing functions with Ord and Random
### 2.2 Issues with numbers and text
#### 2.2.1 Numeric types and type classes
#### 2.2.2 Numeric conversions
#### 2.2.3 Computing with fixed precision
#### 2.2.4 More about Show and Read
#### 2.2.5 Converting recursive types to strings
### 2.3 Abstracting computations with type classes
#### 2.3.1 An idea of a computational context and a common behavior
* See also: unit 5 of Get Programming with Haskell by Will Kurt
#### 2.3.2 Exploring different contexts in parallel
#### 2.3.3 The do notation
#### 2.3.4 Folding and traversing
### Summary
...
## 5 Monads as practical functionality providers
* Uses: 2.3 Abstracting computations with type classes
### 5.1 Basic monads in use: Maybe, Reader, Writer
#### 5.1.1 Maybe monad as a line saver
* Monadic bind
* Monoid operation
* *Functor*
* `fmap`
* *Applicative*
* Monad
#### 5.1.2 Carrying configuration all over the program with Reader
...

## 7 Error handling and logging
1. ex01
* expr/rpn/EvalRPNExcept.hs (lib)
* rpnexpr
    * EvalRPNExcept

2. ex02
* ch07/div.hs
* div

3. ex03
* suntimes/Types.hs
---
* suntimes/Main.hs
    * STExcept
    * App
    * ProcessRequest
* suntimes/App.hs
    * Types
---
* suntimes/STExcept.hs
    * Types
---
* suntimes/ProcessRequest.hs
    * App
    * Types
    * GeoCoordsReq
        * App
        * Types
        * STExcept
    * SunTimes
        * Types
        * STExcept
        * App
    * STExcept
        * Types

4. ex04
* ch07/logging.hs
* logging


# Compilation note
## 2022-05-21 version lts-18
* lts-18 (git comment)
* 'stack.yaml': 'resolver: lts-18.21'
  * no more on the lts list (2022-08)
  * lts-18.8
    * ghc: 8.10.6
    * base: 4.14.3.0
    * fpco: build and tests OK
  * lts-18.28
    * ghc: 8.10.7
    * base: 4.14.3.0
    * openSUSE Tumbleweed: build and tests OK
    * Fedora 34, 35, 36: ghc8.10
* 'packages.yaml':
  * 'tested-with: GHC == 8.6.5, GHC == 8.8.3'
    * GHC == 8.6.5: lts-14.27
    * GHC == 8.8.3: lts-16.11
  * 'dependencies: base >=4.12 && <4.15'
    * means ghc-8.6, ghc-8.8, ghc-8.10
    * ~~lts-19.17~~
      * ghc: 9.0.2
      * ~~base: 4.15.1.0~~ (too recent!)
      * ArchLinux
      * Fedora 34, 35, 36: ghc9.0
    * lts-18.28
    * lts-18.8
    * Fedora Linux 36
      * ghc: 8.10.5
      * build OK, test failing
    * Fedora Linux 35
      * ghc: 8.10.5
    * lts-18.6
      * ghc: 8.10.4
      * base: 4.14.1.0
    * lts-17.2
      * ghc: 8.10.3
      * base-4.14.1.0
    * lts-16.31
      * ghc: 8.8.4
      * base: 4.13.0.0
      * Debian GNU/Linux 11 (bullseye) 2021-08
      * Ubuntu 22.04
      * Fedora 33, 34
    * lts-16.11
      * ghc: 8.8.3
        * cabal build and tests OK
      * base: 4.13.0.0
    * lts-15.3
      * ghc: 8.8.2
      * base: 4.13.0.0
    * lts-14.27
      * ghc: 8.6.5
      * base: 4.12.0.0
      * Ubuntu 20.04
    * lts-13.19
      * ~~ghc: 8.6.4~~ (too old!)
      * base: 4.12.0.0
    * lts-13.11
      * ghc: 8.6.3
      * base: 4.12.0.0
    * ~~lts-12.26~~
      * ghc: 8.4.4
      * ~~base: 4.11.1.0~~ (too old!)



---
---
---
## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/playing-with-the-code-of-the-book-haskell-in-depth/haskell.git
git branch -M main
git push -uf origin main
```

## Integrate with your tools

- [ ] [Set up project integrations](https://gitlab.com/playing-with-the-code-of-the-book-haskell-in-depth/haskell/-/settings/integrations)

## Collaborate with your team

- [ ] [Invite team members and collaborators](https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Enable merge request approvals](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
- [ ] [Automatically merge when pipeline succeeds](https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing(SAST)](https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://docs.gitlab.com/ee/user/clusters/agent/)
- [ ] [Set up protected environments](https://docs.gitlab.com/ee/ci/environments/protected_environments.html)

***

# Editing this README

When you're ready to make this README your own, just edit this file and use the handy template below (or feel free to structure it however you want - this is just a starting point!). Thank you to [makeareadme.com](https://www.makeareadme.com/) for this template.

## Suggestions for a good README
Every project is different, so consider which of these sections apply to yours. The sections used in the template are suggestions for most open source projects. Also keep in mind that while a README can be too long and detailed, too long is better than too short. If you think your README is too long, consider utilizing another form of documentation rather than cutting out information.

## Name
Choose a self-explaining name for your project.

## Description
Let people know what your project can do specifically. Provide context and add a link to any reference visitors might be unfamiliar with. A list of Features or a Background subsection can also be added here. If there are alternatives to your project, this is a good place to list differentiating factors.

## Badges
On some READMEs, you may see small images that convey metadata, such as whether or not all the tests are passing for the project. You can use Shields to add some to your README. Many services also have instructions for adding a badge.

## Visuals
Depending on what you are making, it can be a good idea to include screenshots or even a video (you'll frequently see GIFs rather than actual videos). Tools like ttygif can help, but check out Asciinema for a more sophisticated method.

## Installation
Within a particular ecosystem, there may be a common way of installing things, such as using Yarn, NuGet, or Homebrew. However, consider the possibility that whoever is reading your README is a novice and would like more guidance. Listing specific steps helps remove ambiguity and gets people to using your project as quickly as possible. If it only runs in a specific context like a particular programming language version or operating system or has dependencies that have to be installed manually, also add a Requirements subsection.

## Usage
Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.

## Support
Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.

## Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.

## Contributing
State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## License
For open source projects, say how it is licensed.

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.
